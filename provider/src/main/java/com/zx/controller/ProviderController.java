package com.zx.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.zx.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderController {

    //spel
    @Value("${server.port}")
    String port;

    @Autowired
    private ProviderService providerService;

    @GetMapping("/index")
    public String index(){
        return this.port;
    }


    @GetMapping("/test1")
    public String test1(){
         providerService.test();
         return "test1";
    }

    @GetMapping("/test2")
    public String test2(){
        providerService.test();
        return "test2";
    }


    @GetMapping("/list")
    public String list(){
        return "list";
    }


//    @GetMapping("/hot")
//    @SentinelResource("hot")
//    public String hot(@R){
//        return "list";
//    }



}
