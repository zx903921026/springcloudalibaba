package com.zx.test;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import org.springframework.web.client.RestTemplate;

public class Test {
    public static void main(String[] args) throws Exception {
        //创建消息生产者
        DefaultMQProducer producer = new DefaultMQProducer("myproducer-group");
        //设置NameServer
        producer.setNamesrvAddr("81.68.163.42:9876");
        //启动生产者
        producer.start();
        //构建消息对象
        Message message = new Message("myTopic","myTag",("zzzzzzzzzzzzxxxxxxxxxxxxxxxxxxxx").getBytes());
        //发送消息
        SendResult result = producer.send(message, 1000);
        System.out.println(result);
        //关闭生产者
        producer.shutdown();
    }

//    public static void main(String[] args) throws InterruptedException {
//        RestTemplate restTemplate=new RestTemplate();
//        for (int i = 0; i < 10000; i++) {
//            restTemplate.getForObject("http://localhost:8081/list",String.class);
//            Thread.sleep(200);
//        }
//    }

}


