package com.zx.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
@Log4j2
@RestController
public class ConsumerController {

    @Autowired
    private DiscoveryClient discoveryClient;
    //不会自动装载 需要弄配置类
    @Autowired
    private RestTemplate restTemplate;

    //获取实例
    @GetMapping("getServiceInstance")
    public List<ServiceInstance> getServiceInstance(){
        List<ServiceInstance> provider = this.discoveryClient.getInstances("provider");
        return provider;
    }

    @GetMapping("index")
    public String index(){
        List<ServiceInstance> providerList = this.discoveryClient.getInstances("provider");
        int index= ThreadLocalRandom.current().nextInt(providerList.size());
        ServiceInstance serviceInstance = providerList.get(index);
        String url=serviceInstance.getUri() +"/index";
        String forObject = restTemplate.getForObject(url, String.class);
        log.info("端口:",serviceInstance.getPort());
        return serviceInstance.getPort()+forObject;
    }


    @GetMapping("index2")
    public String index2(){
        return this.restTemplate.getForObject("http://provider/index", String.class);
    }
}
